const express = require("express");
const axios = require("axios");
var cors = require('cors')
var bodyParser = require('body-parser')
var jsonParser = bodyParser.json()

const app = express();
const port = 3000;

app.use(bodyParser.json())
app.use(cors())


app.get("/", (req, res) => {
 res.send("Welcome to activa bypass https api try => /automobile(GET,POST) and /lady_startup(GET,POST)")
});

app.get("/automobile", (req, res) => {
  const url =
    "http://api.group-activa.com/Welios/Product/Product/GetFormControlByProduct_Online/OL-201-01";
  axios.get(url, req.body).then((response) => {
    res.json(response.data);
  }).catch(function (error) {
    // handle error
    console.log(error);
  });
});


app.post("/automobile", (req, res) => {  
  const url =
    "http://api.group-activa.com/Welios/Product/Product/EvaluateCoverageEx_Online?id=OL-201-01";
  axios.post(url, req.body).then((response) => {
    res.json(response.data);
  }).catch(function (error) {
    // handle error
    console.log(error);
  });
});


app.post("/generatePdf", (req, res) => {  
  console.log("Body of data : ",req.body);
  var objectEditionID = req.query.objectEditionID
  var reportLayoutID = req.query.reportLayoutID
  var language = req.query.language
  const url =`http://api.group-activa.com/Welios/Home/GeneratePdfPolicyFromJson?objectEditionID=${objectEditionID}&reportLayoutID=${reportLayoutID}&language=${language}`;
  console.log("this is the url : ",url);
  axios.post(url, req.body,{timeout: 120000}).then((response) => {
    console.log("response : ",response.data);
    res.json(response.data);
  }).catch(function (error) {
    // handle error
    console.log(error);
  });
});





app.get("/lady_startup", (req, res) => {
  const url =
    "http://api.group-activa.com/Welios/Product/Product/GetFormControlByProduct_Online/OL-0C7-01";
  axios.get(url, req.body).then((response) => {
    res.json(response.data);
  }).catch(function (error) {
    // handle error
    console.log(error);
  });
});

app.post("/lady_startup", (req, res) => {
  const url ="http://api.group-activa.com/Welios/Product/Product/EvaluateCoverageEx_Online?id=OL-0C7-01";
  axios.post(url, req.body).then((response) => {
    res.json(response.data);
  }).catch(function (error) {
    // handle error
    console.log(error);
  });


});

app.listen(process.env.PORT || 3000, () => {
  console.log(`app listening at :${port}`);
});
